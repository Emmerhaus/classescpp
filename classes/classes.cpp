﻿#include <iostream>

class Bank {
public:
	Bank() 
	{}

	Bank(double _money): money(_money) 
	{}

	void printMoney() {
		std::cout << money;
	}

private:
	double money = 1000;
};

class Vector {

public:
	Vector() 
	{}

	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	double length() {
		return std::sqrt((x * x + y * y + z * z));
	}

private:
	double x = 0;
	double y = 0;
	double z = 0;
};

int main()
{
	auto vec = Vector(3, 4, 6);
	std::cout << vec.length() << "\n";

	auto bank = Bank(2000);
	bank.printMoney();
}